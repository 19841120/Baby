import React from 'react';
import ReactDOM from 'react-dom';
import { Provider} from 'react-redux'
import { BrowserRouter,Switch,Route} from 'react-router-dom'
import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk'
import reducers from './reducer'
import './index.css';
import './config'
import AuthRoute from "./container/authroute/authroute"
import Login from './container/login/login'
import Register from './container/register/register'
import Dashboard from './container/dashboard/dashboard'

const store = createStore(reducers,compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
))

ReactDOM.render(
    (
        <Provider store={store}>
            <BrowserRouter>
            <div>
                <AuthRoute></AuthRoute>
                <Switch>
                    <Route path="/login" component={Login}></Route>
                    <Route path="/register" component={Register}></Route>
                    <Route component={Dashboard}></Route>
                </Switch>
            </div>
            </BrowserRouter>
        </Provider>
    ), 
    document.getElementById('root')
);

