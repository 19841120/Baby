import React from 'react'
import {NavBar} from 'antd-mobile'
import Navlink from '../navLink/navlink'
import {Switch,Route} from 'react-router-dom'
import Home from '../home/home'
import Sign from '../sign/sign'
import My from '../my/my'

export default class Dashboard extends React.Component {
    render() {
        const navData=[
            {
                title:'首页列表',
                text: '首页',
                icon:'home',
                path:"/home",
                component:Home
            },
            {
                title: '请假提交',
                text: '请假',
                icon: 'sign',
                path: "/sign",
                component: Sign
            }, 
            {
                title: '我的',
                text: '我的',
                icon: 'my',
                path: "/my",
                component: My
            },
        ]
        return (
            <div>
                <div>
                    <div>
                        <NavBar>{navData.find(v => v.path === this.props.location.pathname).title}</NavBar>
                        <Switch>
                            {
                                navData.map(v => (
                                    <Route key={v.path} path={v.path} component={v.component}></Route>
                                ))
                            }   
                        </Switch>
                    </div>
                    <Navlink data={navData}></Navlink>
                </div>
            </div>
        )
    }
}