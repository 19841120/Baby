import React from 'react'
import { TabBar } from 'antd-mobile'
import {withRouter} from 'react-router-dom'

@withRouter
export default class Navlink extends React.Component {
    constructor(props){
        super(props);
        this.state={

        }
    }
    render() {
        const navList = this.props.data
        const pathname=this.props.location.pathname
        return (
            <div className="bot">
                <TabBar >
                {
                    navList.map(v => (
                        <TabBar.Item
                            key={v.text}
                            title={v.text}
                            icon={{ uri: require(`../img/${v.icon}.png`) }}
                            selectedIcon={{ uri: require(`../img/${v.icon}-a.png`) }}
                            selected={v.path===pathname}
                            onPress={
                                () => { this.props.history.push(v.path)}
                            }
                        />
                        ))
                } 
                </TabBar>
            </div>
        )
    }
}