import React from 'react'
import { List, InputItem,WhiteSpace,WingBlank,Button} from 'antd-mobile'
import Logo from '../logo/logo'
import { connect } from 'react-redux'
import { Redirect } from "react-router-dom"
import { register } from '../../redux/user.redux'

@connect(
    state => state.user,
    { register }
)
export default class Register extends React.Component{
    constructor(props){
        super(props);
        this.state={
          user:'',
          pwd:'' ,
          time:''
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(key, val) {
        this.setState({
            [key]: val
        })
    }
    handleSubmit(){
        this.props.register(this.state)
    }
    render(){
        const pathname = this.props.location.pathname
        return (
            <div>
                {this.props.redirect && this.props.redirect !== pathname ? <Redirect to={this.props.redirect} /> : null}
                <Logo></Logo>
                <List> 
                    <InputItem placeholder="请输入姓名" onChange={(v) => this.handleChange('user', v)}>姓名</InputItem>
                    <InputItem placeholder="请输入密码" onChange={(v) => this.handleChange('pwd', v)}>密码</InputItem>  
                </List>
                <WhiteSpace size='lg'></WhiteSpace>
                <WingBlank>
                    <Button type='primary' onClick={this.handleSubmit}>注册</Button>
                </WingBlank>
            </div>
        )
    }
}