import React from 'react'
import { List } from 'antd-mobile';

const Item = List.Item;

const data = [
    {   
        id:1,
        time: '2018.10.23',
        des: '这个周末足球课取消222',
    },
    {
        id: 2,
        time: '2018.10.16',
        des: '周末足球课正常进行',
    },
    {
        id: 3,
        time: '2018.10.08',
        des: '周六课程正常进行，大班周天足球课提到下午上课',
    },
    {
        id: 4,
        time: '2018.10.01',
        des: '国庆长假课程照常进行',
    },
    {
        id: 5,
        time: '2018.09.25',
        des: '由于天气原因，停课',
    }
];



export default class Home extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
           
        };
    }
    render() {  
            return (
                <div>
                    <List renderHeader={() => '消息列表'} >
                        {
                            data.map(v=>(
                                <Item key={v.id} extra={v.time} multipleLine align="top" wrap>
                                    {v.des}
                                </Item>
                            ))
                        }
                        
                    </List>
                </div>
            )
        }
}