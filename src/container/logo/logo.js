import React from 'react'
import imgSrc from '../img/logo.png'


export default class Logo extends React.Component{
    render(){
        return(
            <div>
                <img className="logoImg" src={imgSrc} alt=""/>
                <p className="logoText">Hello，欢迎来到足球小将</p>
            </div>
        )
    }
}