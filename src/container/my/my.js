import React from 'react'
import {Result ,WhiteSpace,WingBlank,Button,List} from 'antd-mobile'
import srcImg from '../img/user.png'
import {connect} from 'react-redux'
import { Redirect} from 'react-router-dom'
import { loginOut} from '../../redux/user.redux'

@connect(
    state=>state.user,
    { loginOut}
)
export default class My extends React.Component {
    constructor(props){
        super(props);
        this.state={

        }
        this.logout = this.logout.bind(this)
    }
    logout(){
        this.props.loginOut()
    }
    componentDidUpdate(){
       console.log(this.props)
    }
    render() {
        const Item = List.Item;
        return (
            <div>
                {this.props.redirect && !this.props.isAuth ? <Redirect to={this.props.redirect} /> : null}
                <Result
                    img={<img src={srcImg} className="userImg" alt=""/>}
                    title={this.props.user}
                />
                <WhiteSpace></WhiteSpace>
                <List renderHeader={() => '上次请假时间'}>
                    {this.props.time ? <Item>{this.props.time}</Item> : <Item>"你很棒，还是全勤哦！"</Item>}   
                </List>
                <WhiteSpace></WhiteSpace>
                <WingBlank>
                    <Button type="primary" onClick={this.logout}>退出登录</Button>
                </WingBlank>
            </div>
        )
    }
}