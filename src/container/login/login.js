import React from 'react'
import { List, InputItem, WhiteSpace, WingBlank, Button } from 'antd-mobile'
import Logo from '../logo/logo'
import {connect} from 'react-redux'
import {Redirect} from "react-router-dom"
import {login} from '../../redux/user.redux'

@connect(
    state=>state.user,
    {login}
)
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user:'',
            pwd:'',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleChange(key,val){
        this.setState({
            [key]:val
        })
    }
    handleSubmit(){
       this.props.login(this.state)
    }
    render() {
        const pathname=this.props.location.pathname
        return (
            <div>
                {this.props.redirect && this.props.redirect !== pathname? <Redirect to={this.props.redirect} />:null }
                <Logo></Logo>
                <List>
                    {this.props.msg ? <p className="warnText">{this.props.msg}</p>:null}
                    <InputItem placeholder="请输入编号" onChange={(v) => this.handleChange('user', v)}>姓名</InputItem>
                    <InputItem placeholder="请输入密码" onChange={(v) => this.handleChange('pwd', v)}>密码</InputItem>
                </List>
                <WhiteSpace size='lg'></WhiteSpace>
                <WingBlank>
                    <Button type='primary' onClick={this.handleSubmit}>登录</Button>
                    <WhiteSpace size='lg'></WhiteSpace>
                    <Button type='primary' onClick={()=>{this.props.history.push('/register')}}>注册</Button>
                </WingBlank>
            </div>
        )
    }
}
export default Login