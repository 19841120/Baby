import React from 'react'
import { List, Calendar,Button,WhiteSpace,WingBlank } from 'antd-mobile';
import enUS from 'antd-mobile/lib/calendar/locale/en_US';
import zhCN from 'antd-mobile/lib/calendar/locale/zh_CN';
import { connect } from 'react-redux'
import { updata } from '../../redux/user.redux'

const extra = {
    '2017/07/15': { info: 'Disable', disable: true },
};

const now = new Date();
extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 5)] = { info: 'Disable', disable: true };
extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 6)] = { info: 'Disable', disable: true };
extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 7)] = { info: 'Disable', disable: true };
extra[+new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8)] = { info: 'Disable', disable: true };

Object.keys(extra).forEach((key) => {
    const info = extra[key];
    const date = new Date(key);
    if (!Number.isNaN(+date) && !extra[+date]) {
        extra[+date] = info;
    }
});

@connect(
    state => state.user,
    { updata }
)
export default class Sign extends React.Component {
    originbodyScrollY = document.getElementsByTagName('body')[0].style.overflowY;
    constructor(props) {
        super(props);
        this.state = {
            en: false,
            show: false,
            config: {},
            time:""
        };
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleSubmit(){
        this.props.updata(this.state.time)
    }
    renderBtn(zh, en, config = {}) {
        config.locale = this.state.en ? enUS : zhCN;

        return (
            <List.Item arrow="horizontal"
                onClick={() => {
                    document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
                    this.setState({
                        show: true,
                        config,
                    });
                }}
            >
                {this.state.en ? en : zh}
            </List.Item>
        );
    }

    onSelectHasDisableDate = (dates) => {
        console.warn('onSelectHasDisableDate', dates);
    }
    onConfirm = (startTime, endTime) => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            show: false,
            startTime,
            endTime,
            time: (startTime.toLocaleString().slice(0, 10)).toString()
        });
    }

    onCancel = () => {
        document.getElementsByTagName('body')[0].style.overflowY = this.originbodyScrollY;
        this.setState({
            show: false,
            startTime: undefined,
            endTime: undefined,
        });
    }

    getDateExtra = date => extra[+date];
    
    render() {
        const tit = this.state.startTime ? <div>{this.state.startTime.toLocaleString()}</div>:'选择日期'
        return (
            <div>
                <List className="calendar-list" style={{ backgroundColor: 'white' }}>
                    {this.renderBtn(tit, 'Select Date', { type: 'one' })}
                </List>
                <Calendar
                    {...this.state.config}
                    visible={this.state.show}
                    onCancel={this.onCancel}
                    onConfirm={this.onConfirm}
                    onSelectHasDisableDate={this.onSelectHasDisableDate}
                    getDateExtra={this.getDateExtra}
                    defaultDate={now}
                    minDate={new Date(+now - 5184000000)}
                    maxDate={new Date(+now + 31536000000)}
                />
                <WhiteSpace></WhiteSpace>
                <div className="signTj">
                    <WingBlank>
                        <Button type="primary" onClick={this.handleSubmit}>提交</Button>
                    </WingBlank>
                </div>    
            </div>
        );
    }
}