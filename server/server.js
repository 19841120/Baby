const express=require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

const app=express()
const userRouter=require('./user')


app.use(cookieParser())
app.use(bodyParser.json())
app.use('/user', userRouter)

app.listen(9093,function(){
    console.log('node in football')
})